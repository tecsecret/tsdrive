<?php 
$OC_Version = array(21,0,0,17);
$OC_VersionString = '21.0.0 RC2';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '20.0' => true,
    '21.0' => true,
  ),
  'owncloud' => 
  array (
    '10.5' => true,
  ),
);
$OC_Build = '2021-02-11T08:33:05+00:00 1afa1360608cf5bdd8f197c523ceb21d92c9d758';
$vendor = 'nextcloud';
